﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class MainMenue : MonoBehaviour
{
    TMP_InputField Choises;
    int act;
    TMP_InputField[] inputFields;

    private void Start()
    {
        inputFields = FindObjectsOfType<TMP_InputField>();
        act = inputFields.Length -1;

        for (int i = inputFields.Length - 1; i >= 0; i--) {
            inputFields[i].placeholder.color = Color.clear;
        }

        inputFields[act].placeholder.color = Color.gray;
    }
    public void AddChoiseForm()
    {
        inputFields[act].placeholder.color = Color.gray;
        act--;
    }

    public void TurnScene()
    {
        
    }
}
