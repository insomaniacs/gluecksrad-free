﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Author: Nico Mahler
//Date: 09.09.2019

public class InputController : MonoBehaviour
{

    public Transform MouseTransform;
    public float force;
    public Text DegreesText;

    public float minForceToSetSpinned;

    bool spinned = false;


    //drag an drop events for shooting
    //for the Inspector to set the max Range between start/stop point of mouse drag and clamp between those
    [SerializeField]
    float maxSpin = 5;

    bool leftPressed = false;
    bool rightPressed = false;

    Vector2 pressedPos;
    Vector2 releasedPos;
    Vector2 shootDirection = new Vector2(0, 1);
    float spinVelocity = 0;

    //Look Direction for Animator
    Vector2 lookDirection = new Vector2(0, 1);

    Rigidbody rb;
    Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
        ProcessInput();
    }

    private void GetInput()
    {

        if (!spinned)
        {
            //Shooting Events
            GetMouseSpin();
        }
    }

    private void SetText()
    {
        DegreesText.text = "" + Mathf.Round(Mathf.Abs(Vector2.Angle(Vector2.up, shootDirection))) + "°";
    }

    private void ProcessInput()
    {

    }

    private void GetMouseSpin()
    {
        //if any other action is pressed, return
        if (rightPressed)
        {
            return;
        }

        //the moment i pressed the shoot button
        if (Input.GetMouseButtonDown(0) && !leftPressed)
        {
            pressedPos = GetLocalMouseInputPosition();
            leftPressed = true;

            //enable UI element Text
            DegreesText.enabled = true;

            //Debug.Log("pressed!");
        }

        //during drag
        if (Input.GetMouseButton(0) && leftPressed)
        {
            releasedPos = GetLocalMouseInputPosition();
            //Debug.Log("dragged");
            //calculate shoot Direction for look direction and visual Feedback
            CalculateSpin();

            //show degrees
            SetText();
        }

        //the moment i release the shoot button
        if (Input.GetMouseButtonUp(0) && leftPressed)
        {
            releasedPos = GetLocalMouseInputPosition();
            //Debug.Log("released");
            //calculate Shoot again for precise Shooting
            CalculateSpin();

            //set shoot Event true


            leftPressed = false;


            //hide UI Element
            DegreesText.enabled = false;
        }

    }


    private Vector2 GetLocalMouseInputPosition()
    {
        Vector2 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        position.x -= transform.position.x;
        position.y -= transform.position.y;

        return position;
    }

    private Vector2 GetWorldMouseInputPosition()
    {
        Vector2 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        return position;
    }

    private void CalculateSpin()
    {

        //calculate the shooting direction
        shootDirection = (pressedPos - releasedPos);
        //set the look direction
        lookDirection = shootDirection.normalized;


        //calculate the shooting veloctiy
        //Clamp between 0 and max Distance between start and releasing point to ensure control & divide by maxDistance for clamping between 0 and 1
        spinVelocity = Mathf.Clamp(shootDirection.magnitude, 0, maxSpin) / maxSpin;

        //Debug.Log(spinVelocity);

        if (spinVelocity >= 0.9) spinned = true;

        rb.AddForceAtPosition(-shootDirection * force, releasedPos);
        anim.SetTrigger("spin");
    }

}

